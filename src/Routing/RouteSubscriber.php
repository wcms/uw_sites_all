<?php

namespace Drupal\uw_sites_all\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Change title for content creation.
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('workbench.all_content')) {
      $route->setDefault('_title', 'Manage Content');
    }
  }

}
